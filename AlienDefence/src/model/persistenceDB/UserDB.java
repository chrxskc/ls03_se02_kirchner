package model.persistenceDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.User;
import model.persistence.IUserPersistence;

/**
 * databaseconnection for userobjects, Story usermanagement
 * @author Clara Zufall
 * TODO finish this class
 */
public class UserDB implements IUserPersistence{

	private AccessDB dbAccess;
		
	public UserDB(AccessDB dbAccess) {
		super();
		if (dbAccess != null)
			this.dbAccess = dbAccess;
		else
			this.dbAccess = new AccessDB();
	}

	public int createUser(User user) {
		String sql = "INSERT INTO users (P_user_id, first_name, sur_name, birthday, street, house_number, postal_code, city, login_name, password, salary_expectations, marital_status, final_grade) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
		int lastUser = -1;
		return lastUser;
	}
	
	/**
	 * read userdata by unique username
	 * 
	 * @param username
	 * @return userobject, null if user didn't exists
	 */
	
	public User readUser(String username) {
		String sql = "SELECT * FROM users WHERE login_name = ? ;";
		User user = null;
		try (Connection con = DriverManager.getConnection(this.dbAccess.getFullURL(), this.dbAccess.getUser(),
				this.dbAccess.getPassword()); PreparedStatement statement = con.prepareStatement(sql)) {

			statement.setString(1, username);

			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				user = new User(rs.getInt("P_user_id"), rs.getString("first_name"), rs.getString("sur_name"),
						rs.getDate("birthday").toLocalDate(), rs.getString("street"), rs.getString("house_number"),
						rs.getString("postal_code"), rs.getString("city"), rs.getString("login_name"),
						rs.getString("password"), rs.getInt("salary_expectations"), rs.getString("marital_status"),
						rs.getBigDecimal("final_grade").doubleValue());
			}

		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}

		return user;
	}

	public void updateUser (User user) {
		
	}
	
	public void deleteUser (User user) {
		String sql = "DELETE FROM users WHERE P_user_id = " + user + ";";
		try (Connection con = DriverManager.getConnection(this.dbAccess.getFullURL(), this.dbAccess.getUser(),
				this.dbAccess.getPassword()); Statement statement = con.createStatement()) {
			statement.executeUpdate(sql);
		}	catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
